package com.bogor.smartgercep.ui.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bogor.smartgercep.network.Resource
import com.bogor.smartgercep.repository.AreaRepository
import com.bogor.smartgercep.repository.AuthRepository
import com.bogor.smartgercep.responses.LoginResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val authRepository: AuthRepository
):ViewModel() {

    private val _loginResponse : MutableLiveData<Resource<LoginResponse>> = MutableLiveData()

    val loginResponse: LiveData<Resource<LoginResponse>>
        get() = _loginResponse

    private var _emailInput : MutableLiveData<String>? = MutableLiveData()
    val emailInput: LiveData<String>?
        get() = _emailInput


    fun setEmail(email: String) {
        _emailInput?.value=email
    }

    fun login(
        email: String,
        password: String
    )=viewModelScope.launch {
        _loginResponse.value = authRepository.login(
            email, password
        )
    }
}