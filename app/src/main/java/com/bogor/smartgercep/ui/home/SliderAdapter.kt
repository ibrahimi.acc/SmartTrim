package com.bogor.smartgercep.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bogor.smartgercep.R

class SliderAdapter(private val sliderItem: List<SliderItem>): RecyclerView.Adapter<SliderAdapter.SliderViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderViewHolder {
        return SliderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.slider_item_home,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SliderViewHolder, position: Int) {
        holder.bind(sliderItem[position])
    }

    override fun getItemCount(): Int {
        return sliderItem.size
    }
    inner class SliderViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val imageView = view.findViewById<ImageView>(R.id.imageViewHome)

        fun bind(sliderItem: SliderItem){
            imageView.setImageResource(sliderItem.icon)
        }
    }



}