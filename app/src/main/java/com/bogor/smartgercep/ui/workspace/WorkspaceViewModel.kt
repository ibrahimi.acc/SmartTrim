package com.bogor.smartgercep.ui.workspace

import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import com.bogor.smartgercep.model.AreaModel
import com.bogor.smartgercep.model.WorkspaceModel
import com.bogor.smartgercep.network.Resource
import com.bogor.smartgercep.repository.AreaRepository
import com.bogor.smartgercep.repository.WorkspaceRepository
import com.bogor.smartgercep.responses.AreaResponse
import com.bogor.smartgercep.responses.WorkspaceResponse
import com.bogor.smartgercep.ui.users.UsersViewModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import dagger.hilt.android.lifecycle.HiltViewModel
import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.createObject
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject


@HiltViewModel
class WorkspaceViewModel @Inject constructor(
    private  val workspaceRepository: WorkspaceRepository
) : ViewModel() {
    private var realm: Realm? = null
    private val _workspaceResponse : MutableLiveData<Resource<WorkspaceResponse>> = MutableLiveData()
    val workspaceResponse: LiveData<Resource<WorkspaceResponse>>
        get() = _workspaceResponse

    var _workspaceData : MutableLiveData<List<WorkspaceModel>> = MutableLiveData()
    val workspaceData : LiveData<List<WorkspaceModel>>
        get() = _workspaceData
    init {
        realm = Realm.getDefaultInstance()
        getWorkspacedata()
    }
    fun getWorkspacedata(){
        val workspace = realm?.where(WorkspaceModel::class.java)?.sort("createAt", Sort.DESCENDING)?.findAll()
        _workspaceData.value = workspace
    }
    fun saveWorkspace(
        workspaceName:String,
        wilayahId:String,
        desaName:String,
        rt:String,
        rw:String
    ){
        var workspaceModel : WorkspaceModel? = null
        val idWorkspace = UUID.randomUUID().toString()
        realm?.executeTransaction { realmTransaction ->
            workspaceModel = realmTransaction.createObject(idWorkspace)
            workspaceModel!!.workspaceName=workspaceName
            workspaceModel!!.wilayahId=wilayahId
            workspaceModel!!.desaName=desaName
            workspaceModel!!.rt = rt
            workspaceModel!!.rw = rw
            realmTransaction.insertOrUpdate(workspaceModel)
        }
        getWorkspacedata()
    }
    override fun onCleared() {
        realm!!.close()
        super.onCleared()
    }
}
class WorkspaceViewModelFactory @Inject constructor(private val workspaceRepository: WorkspaceRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return WorkspaceViewModel(workspaceRepository) as T
    }
}