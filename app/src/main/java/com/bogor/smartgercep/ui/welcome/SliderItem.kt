package com.bogor.smartgercep.ui.welcome

data class SliderItem(
    val title: String,
    val subTitile: String,
    val icon: Int
)