package com.bogor.smartgercep.ui.workspace

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.text.set
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bogor.smartgercep.R
import com.bogor.smartgercep.databinding.FragmentWorkspaceBinding
import com.bogor.smartgercep.model.AreaModel
import com.bogor.smartgercep.network.Resource
import com.bogor.smartgercep.ui.maps.MapsActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WorkspaceFragment : Fragment(),WorkspaceAdapater.OnItemClickListener {

    private var _binding: FragmentWorkspaceBinding? = null
    private val areaModel: AreaViewModel by viewModels()
    private val workspaceModel :WorkspaceViewModel by viewModels()
    private val binding get() = _binding!!
    private lateinit var materialAlertDialogBuilder: MaterialAlertDialogBuilder
    private lateinit var nameTextField : TextInputLayout
    private lateinit var kecamatanTextField : TextInputLayout
    private lateinit var desaTextField : TextInputLayout
    private lateinit var customAlertDialogView : View
    private lateinit var wilayahId : String
    lateinit var workspaceAdapater: WorkspaceAdapater
    val lm = LinearLayoutManager(activity)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentWorkspaceBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initView()
        materialAlertDialogBuilder = MaterialAlertDialogBuilder(requireContext())
        workspaceModel.workspaceData.observe(viewLifecycleOwner,{
            if (it.size>0){
                binding.viewWorkspacebackgournd.visibility=View.GONE
            }
            workspaceAdapater.setWorkspace(it)


        })
        areaModel.areaResponse.observe(viewLifecycleOwner,{
            when(it){
                is Resource.Success -> {
                    areaModel.saveAreaFromDb(it.value)
                }
                is Resource.Failure ->{
                    Log.d("asd","${it.errorCode}")
                }
            }
        })
        binding.createWorkspace.setOnClickListener {
            customAlertDialogView = LayoutInflater.from(requireContext())
                .inflate(R.layout.create_workspace_form, null, false)
            openDialog()
        }
        return root
    }
    override fun onItemClick(position: Int) {

        val uid = workspaceAdapater.getUId(position)
        Log.d("workspace_id","${uid}")
        activity?.let{
            val intent = Intent (it, MapsActivity::class.java)
            intent.putExtra("idWorkspace",uid.toString())
            it.startActivity(intent)
        }
    }
    private fun initView() {
        binding.recycleViewWorkspace.layoutManager = lm
        workspaceAdapater= WorkspaceAdapater(requireActivity(),this)
        binding.recycleViewWorkspace.adapter = workspaceAdapater

    }
    fun openDialog(){
        nameTextField = customAlertDialogView.findViewById(R.id.workspaceName)
        kecamatanTextField = customAlertDialogView.findViewById(R.id.kecamatanName)
        desaTextField = customAlertDialogView.findViewById(R.id.desaName)
        val workspaceRT : TextInputLayout = customAlertDialogView.findViewById(R.id.workspaceRT)
        val workspaceRW : TextInputLayout = customAlertDialogView.findViewById(R.id.workspaceRW)
        val items = areaModel.areaData.filter { it.tipe=="kecamatan" }
        val adapterKecamatan = ArrayAdapter(requireContext(), R.layout.list_item_kecamatan, items)
        val kecamatanField = kecamatanTextField.editText as? AutoCompleteTextView
        val desaTextFields = desaTextField.editText as? AutoCompleteTextView
        kecamatanField?.setAdapter(adapterKecamatan)
        kecamatanField?.onItemClickListener=object : AdapterView.OnItemClickListener{
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                desaTextFields?.text?.clear()
                val idKecamatan = kecamatanField?.adapter?.getItem(position) as AreaModel
                Log.d("ads","${areaModel.areaData.filter { it.induk=="${idKecamatan.id}" }}")
                val itemDesa = areaModel.areaData.filter { it.induk=="${idKecamatan.id}" }
                val adapterDesa = ArrayAdapter(requireContext(), R.layout.list_item_kecamatan, itemDesa)
                desaTextFields?.setAdapter(adapterDesa)
                desaTextFields?.setSelection(0)
                desaTextFields?.setText(itemDesa[0].nama,false)
                val idDesa = desaTextFields?.adapter?.getItem(0) as AreaModel
                wilayahId=idDesa._id.toString()
            }
        }
        desaTextFields?.onItemClickListener=object:AdapterView.OnItemClickListener{
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val idDesa = desaTextFields?.adapter?.getItem(position) as AreaModel
                wilayahId=idDesa._id.toString()
            }

        }
        materialAlertDialogBuilder.setView(customAlertDialogView)
            .setTitle("Buat Ruang Kerja")
            .setPositiveButton("Tambah") { dialog, _ ->
                val workspaceName = nameTextField.editText?.text.toString()
                val kecamatanName = kecamatanTextField.editText?.text.toString()
                val desaName = desaTextField.editText?.text.toString()
                val workspaceRT = workspaceRT.editText?.text.toString()
                val workspaceRW = workspaceRW.editText?.text.toString()
                if (workspaceName.isEmpty()||kecamatanName.isEmpty()||desaName.isEmpty()||workspaceRT.isEmpty()||workspaceRW.isEmpty())
                    createSnackBar("Semua isian tidak boleh kosong")
                else
                    workspaceModel.saveWorkspace(workspaceName,wilayahId,desaName,workspaceRT,workspaceRW)
            }
            .setNegativeButton("Batal") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }
    fun createSnackBar(msg:String){
        Snackbar.make(
            binding.workspaceContext,
            msg,
            Snackbar.LENGTH_SHORT
        ).show()
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}