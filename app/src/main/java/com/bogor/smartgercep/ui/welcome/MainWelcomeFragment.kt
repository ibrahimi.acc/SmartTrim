package com.bogor.smartgercep.ui.welcome

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionScene
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.bogor.smartgercep.R
import com.bogor.smartgercep.databinding.FragmentMainWelcomeBinding
import com.google.android.material.transition.MaterialSharedAxis


class MainWelcomeFragment : Fragment() {
    private var _binding: FragmentMainWelcomeBinding?  = null
    private val binding get() = _binding!!
    lateinit var motionTransition: MotionScene
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, /* forward= */ true)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, /* forward= */ false)
        reenterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, /* forward= */ false)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMainWelcomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
//        motionTransition=MotionScene()

        binding.button.setOnClickListener {
            (activity as WelcomeActivity?)?.OpenFragment()
//            Navigation.findNavController(root).navigate(R.id.loginFragment)
            val extras = FragmentNavigatorExtras((view to "shared_element_container") as Pair<View, String>)
            findNavController().navigate(R.id.loginFragment, null, null, extras)

        }

        return root
    }

}