package com.bogor.smartgercep.ui.maps

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bogor.smartgercep.model.PointModel
import com.bogor.smartgercep.model.GeometryModel
import com.bogor.smartgercep.model.WorkspaceModel
import com.bogor.smartgercep.util.distanceTo
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polygon
import io.realm.Realm
import io.realm.kotlin.createObject
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
class MapViewModel(
    private val idWorkspace: String
) : ViewModel() {
    private var realm: Realm? = null
    var _geometryData : MutableLiveData<List<GeometryModel>> = MutableLiveData()

    val geometryData : LiveData<List<GeometryModel>>
        get() = _geometryData
    init {
        realm = Realm.getDefaultInstance()
        getAllData()
    }
    fun getAllData(){
        val data = realm?.where(GeometryModel::class.java)?.equalTo("workspace.id",idWorkspace)?.findAll()
        _geometryData.value= data
    }
    fun savePolygon(
        idWorkspace: String,
        map: Polygon,
        gpsPosition: LatLng,
        altitude: Double,
        type: String
    ): String {
        val workspaceUid = realm?.where(WorkspaceModel::class.java)
            ?.equalTo("id", idWorkspace)?.findFirst()
        Log.d("asd","${workspaceUid}")
        var geometry: GeometryModel? = null
        val uidPolygon = UUID.randomUUID().toString()

        realm!!.executeTransaction { realm ->
            geometry = realm.createObject(uidPolygon)
            geometry!!.workspace = workspaceUid
            geometry!!.type = type
            var number = 1
            var jsonPoint = JSONArray()
            map.points.forEach {
                val point = realm.createObject<PointModel>(UUID.randomUUID().toString())
                var jsonParams = JSONObject()
                jsonParams.put("lat", it.latitude)
                jsonParams.put("lon", it.longitude)
                if (gpsPosition != null)
                    point.distanceToGps =
                        LatLng(it.latitude, it.longitude).distanceTo(gpsPosition!!).toDouble()
                point.lat = it.latitude
                point.lon = it.longitude
                point.altitude = altitude
                point.createAt = Date()
                point.updateAt = Date()
                point.number = number
                number++
                geometry!!.points!!.add(point)
                jsonPoint.put(jsonParams)
            }
        }
        return uidPolygon
    }
}
class MapViewModelFactory(
    private val idWorkspace: String
): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MapViewModel(idWorkspace) as T
    }
}