package com.bogor.smartgercep.ui.users

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.viewpager2.widget.ViewPager2
import com.bogor.smartgercep.R
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UsersActivity : AppCompatActivity() {
    lateinit var tabLayout: TabLayout
    lateinit var viewPager2: ViewPager2
    lateinit var adapter: FragmentUserAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        tabLayout = findViewById<TabLayout>(R.id.tabLayoutUsers)
        viewPager2 = findViewById<ViewPager2>(R.id.viewPagerUser)
        val fragmentManager: FragmentManager = supportFragmentManager
        adapter = FragmentUserAdapter(fragmentManager, lifecycle)
        viewPager2.adapter = adapter

        tabLayout!!.addTab(tabLayout.newTab().setText("Masuk"))
        tabLayout!!.addTab(tabLayout.newTab().setText("Daftar"))

        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                tabLayout.selectTab(tabLayout.getTabAt(position))
            }
        })

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager2!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }
    fun changetoRegister(id:Int){
        viewPager2!!.currentItem = id
    }
}