package com.bogor.smartgercep.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bogor.smartgercep.R
import com.bogor.smartgercep.databinding.FragmentHomeBinding
import com.bogor.smartgercep.ui.location.PickLocationActivity
import com.google.android.material.appbar.AppBarLayout
import qiu.niorgai.StatusBarCompat.changeToLightStatusBar
import qiu.niorgai.StatusBarCompat.setStatusBarColorForCollapsingToolbar


class HomeFragment : Fragment(), AppBarLayout.OnOffsetChangedListener {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        Log.d("asd", binding.toolbar.hasExpandedActionView().toString())
        val appBarLayout = root!!.findViewById(R.id.appBarLayout) as AppBarLayout
        appBarLayout.addOnOffsetChangedListener(this)
        setStatusBarColorForCollapsingToolbar(requireActivity(), binding.appBarLayout, binding.CollapToolBarLayout,
            binding.toolbar, Color.TRANSPARENT)
        changeToLightStatusBar(requireActivity())

        binding.selectLocation.setOnClickListener {
            val intent = Intent(requireContext(), PickLocationActivity::class.java)
            startActivity(intent)
        }
        return root
    }

    @SuppressLint("ResourceType")
    override fun onOffsetChanged(appBarLayout: AppBarLayout?, offset: Int) {
        if (offset == 0) {
            // Fully expanded
            binding.icCoornodator.setBackgroundColor(getResources().getColor(R.color.customColorSecondary))
            binding.searchBarHome.visibility=View.VISIBLE
        } else {
            binding.icCoornodator.setBackgroundColor(getResources().getColor(R.color.white))
//            Log.d("asd","${offset}")
            if (offset < -120)
                binding.searchBarHome.visibility=View.GONE
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}