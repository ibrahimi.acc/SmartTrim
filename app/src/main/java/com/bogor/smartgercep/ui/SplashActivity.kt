package com.bogor.smartgercep.ui

import android.app.ActivityOptions
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.bogor.smartgercep.DataStoreViewModel
import com.bogor.smartgercep.HomeActivity
import com.bogor.smartgercep.R
import com.bogor.smartgercep.TourActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    private val dataStoreViewModel: DataStoreViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        dataStoreViewModel.hasTour.observe(this){
            if (it == true){
                finish()
                //saved go to the next activity
                val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this@SplashActivity).toBundle())
            }else{
                finish()
                val intent = Intent(this@SplashActivity, TourActivity::class.java)
                startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this@SplashActivity).toBundle())
            }
        }
    }

}