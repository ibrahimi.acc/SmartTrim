package com.bogor.smartgercep.ui.location

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import com.bogor.smartgercep.R
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.ktx.widget.PlaceSelectionError
import com.google.android.libraries.places.ktx.widget.PlaceSelectionSuccess
import com.google.android.libraries.places.ktx.widget.placeSelectionEvents
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import qiu.niorgai.StatusBarCompat

class PickLocationActivity : AppCompatActivity() {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_location)
        val closebtn = findViewById<Toolbar>(R.id.topAppBarLocation)
        StatusBarCompat.setStatusBarColor(this, Color.TRANSPARENT)

        StatusBarCompat.changeToLightStatusBar(this)
        closebtn.setNavigationOnClickListener {
            finish()
        }
        if(!Places.isInitialized())
            Places.initialize(applicationContext, "AIzaSyBH_liriGJrNKQdMkW2viONewt6j2FEuBA");
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME))

        // Listen to place selection events
        lifecycleScope.launchWhenCreated {
            autocompleteFragment.placeSelectionEvents().collect { event ->
                when (event) {
                    is PlaceSelectionSuccess -> Toast.makeText(
                        this@PickLocationActivity,
                        "Got place '${event.place.name}'",
                        Toast.LENGTH_SHORT
                    ).show()
                    is PlaceSelectionError -> Log.d("asd",
                        "Failed to get place '${event.status.statusMessage}'",
                    )

                }
            }
        }
    }
}