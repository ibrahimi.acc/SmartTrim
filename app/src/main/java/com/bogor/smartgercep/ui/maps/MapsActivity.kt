package com.bogor.smartgercep.ui.maps

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.PopupMenu
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bogor.smartgercep.R
import com.bogor.smartgercep.databinding.ActivityMapsBinding
import com.bogor.smartgercep.util.defaultMarker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import qiu.niorgai.StatusBarCompat

class MapsActivity : AppCompatActivity(), OnMapReadyCallback,GoogleMap.OnMapClickListener, GoogleMap.OnMarkerDragListener, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    var pickMode :Int? = null
    var isEditMode : Boolean= false
    lateinit var shapeMode : String
    private var dataPolygon: Polygon? = null
    private var dataPolyline: Polyline? = null
    private var dataPoint: Point? = null
    private var currenctCollectTionPoint : MutableList<LatLng> = mutableListOf()
    private var polygonCollection : MutableList<Polygon> = mutableListOf()
    var POLYGON_COLOR: Int = Color.parseColor("#80ffee00")
    lateinit var menuMap : Menu
    private lateinit var idWorkspace: String
    private val circleMarkersPolygon: MutableList<Marker> = mutableListOf()
    lateinit var mapViewModel : MapViewModel
    companion object {
        private const val ASK_PERMISSIONS_REQUEST_CODE = 1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        StatusBarCompat.setStatusBarColor(this, Color.TRANSPARENT)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        askForPermissions()
        idWorkspace = intent.getStringExtra("idWorkspace").toString()
        mapViewModel= ViewModelProvider(
            this,
            MapViewModelFactory(idWorkspace)
        ).get(
            MapViewModel::class.java
        )
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val menu = binding.topAppBar.menu
        menu.findItem(R.id.changeMaps).setOnMenuItemClickListener {
            showMenu(findViewById(R.id.changeMaps),R.menu.layer_menu)
            true
        }
        menu.findItem(R.id.discardShape).setOnMenuItemClickListener {
            currenctCollectTionPoint.clear()
            circleMarkersPolygon.forEach{
                it.remove()
            }

            circleMarkersPolygon.clear()
            dataPolygon?.remove()
            dataPolygon = mMap.addPolygon(
                PolygonOptions()
                    .strokeWidth(1f)
                    .zIndex(1f)
                    .fillColor(POLYGON_COLOR).strokeWidth(1f)
                    .add(mMap?.cameraPosition?.target)
            )
            true
        }
        menu.findItem(R.id.saveShape).setOnMenuItemClickListener {
            if(currenctCollectTionPoint.size >2){
                val savePolygon = mapViewModel.savePolygon(idWorkspace,dataPolygon!!, LatLng(0.1,0.1),0.4,shapeMode)
                createPolygon(currenctCollectTionPoint.toSet(),savePolygon)
                currenctCollectTionPoint.clear()
                circleMarkersPolygon.forEach{
                    it.remove()
                }
                circleMarkersPolygon.clear()
            }
            true
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    private fun createPolygon(points: Set<LatLng>, idPolygon: String): Polygon? {

        val polygon = mMap?.addPolygon(
            PolygonOptions()
                .add(*points.toTypedArray())
                .strokeWidth(1f)
                .zIndex(1f)
                .fillColor(POLYGON_COLOR).strokeWidth(2f)
                .clickable(true)
                .zIndex(2f)

        )
        polygon.tag=idPolygon
        return polygon
    }
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        menuMap = binding.topAppBar.menu
        mMap = googleMap
        val SYDNEY = LatLng(-6.597629, 106.799568)
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SYDNEY, 14.0f))
        mapViewModel.geometryData.observe(this,{
            it.forEach {
                if(it.type=="Polygon"){
                    val polygon = it.points?.map { point ->
                        LatLng(point.lat!!, point.lon!!)
                    }?.toSet()
                    createPolygon(polygon!!, it.uid.toString())
                    Log.d("asd","${polygon}")
                }
            }
        })
        mMap.apply {
            isMyLocationEnabled = true
        }
        binding.createPoint.setOnClickListener {
            pickMenu("Point")
        }
        binding.createPolygon.setOnClickListener {
            pickMenu("Polygon")
        }
        binding.createPolyline.setOnClickListener {
            pickMenu("Polyline")
        }
        dataPolygon = mMap.addPolygon(
            PolygonOptions()
                .strokeWidth(1f)
                .zIndex(1f)
                .fillColor(POLYGON_COLOR).strokeWidth(1f)
                .add(mMap?.cameraPosition?.target)
        )
        dataPolyline = mMap.addPolyline(
            PolylineOptions()
                .zIndex(1f)
                .add(mMap?.cameraPosition?.target)
        )


//        Listener
        mMap?.setOnMapClickListener(this)
        mMap?.setOnMarkerClickListener(this)
        mMap?.setOnMarkerDragListener(this)
    }


    private fun showMenu(v: View, @MenuRes menuRes: Int) {
        val popup = PopupMenu(applicationContext!!, v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when(menuItem.itemId){
                R.id.mapNormal ->{
                    mMap.apply {
                        mapType = GoogleMap.MAP_TYPE_NORMAL
                    }
                }
                R.id.mapSatellite ->{
                    mMap.apply {
                        mapType = GoogleMap.MAP_TYPE_SATELLITE
                    }
                }
                R.id.terrain ->{
                    mMap.apply {
                        mapType = GoogleMap.MAP_TYPE_TERRAIN
                    }
                }
            }
            true
        }
        popup.setOnDismissListener {
            // Respond to popup being dismissed.
        }
        // Show the popup menu.
        popup.show()
    }
    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(ASK_PERMISSIONS_REQUEST_CODE)
    private fun askForPermissions() {
        // Enable the location layer. Request the location permission if needed.
        val permissions: Array<String>
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            permissions = arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.FOREGROUND_SERVICE,
                Manifest.permission.CAMERA
            )
        } else {
            permissions = arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.CAMERA
            )
        }

        if (EasyPermissions.hasPermissions(this, *permissions)) {
//            Log.i(localClassName, "ALL PERMISSIONS grandted")
        } else {
            // if permissions are not currently granted, request permissions
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.permission_rationale_location),
                ASK_PERMISSIONS_REQUEST_CODE, *permissions
            )
        }
    }
    fun showOnEdit(){
        binding.topAppBar.menu.setGroupVisible(R.id.groupSave,true)

    }

    fun pickMenu(shape:String){
        val items = arrayOf("Manual Measuring", "Pointer", "GPS Measuring")

        MaterialAlertDialogBuilder(this)
            .setTitle("Pilih Mode")
            .setItems(items) { dialog, which ->
                pickMode = which
                shapeMode = shape
                isEditMode = true
                isEditMode = true
                showOnEdit()
            }
            .show()
    }
    fun setPolygonListener(p0: LatLng){
        mMap.apply {
            val number = circleMarkersPolygon.size + 1
            circleMarkersPolygon.add(
                addMarker(
                    defaultMarker(number)
                        .position(p0)
                        .draggable(true)
                )
            )
            currenctCollectTionPoint.add(p0)
            dataPolygon!!.points = currenctCollectTionPoint

        }
    }
    fun setPointListener(p0:LatLng){

    }
    fun setPolylineListener(p0:LatLng){

    }

    override fun onMapClick(p0: LatLng?) {
        if (isEditMode){
            if(shapeMode=="Polygon"){
                Log.d("asd","POLYGON")
                if (pickMode==0) {
                    p0?.let {
                        this.setPolygonListener(it)
                    }
                }
            }
        }else{

        }
    }

    override fun onMarkerDragStart(p0: Marker?) {

    }

    override fun onMarkerDrag(p0: Marker?) {

    }

    override fun onMarkerDragEnd(p0: Marker?) {

        if (isEditMode) {
            val pointLocation = p0!!.title.toInt().minus(1)
            currenctCollectTionPoint[pointLocation]= p0.position
            dataPolygon!!.points = currenctCollectTionPoint
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {

        return true
    }
}