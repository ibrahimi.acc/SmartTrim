package com.bogor.smartgercep.ui.welcome

import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import com.bogor.smartgercep.R
import dagger.hilt.android.AndroidEntryPoint
import net.cachapa.expandablelayout.ExpandableLayout

@AndroidEntryPoint
class WelcomeActivity : AppCompatActivity()  {
    var motionLayout: ConstraintSet? = null
    lateinit var imageView5: ImageView
    lateinit var fadeOut : Animation
    lateinit var fadeIn : Animation
    lateinit var viewImage: View
    lateinit var backgoundAll: View
    private var isInMenu : Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
    with(window) {
        requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)

    }
        super.onCreate(savedInstanceState)
        fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out)
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        setTheme(R.style.Theme_SmartTrim)
//        motionLayout = ConstraintSet().
        supportActionBar?.hide()
        setContentView(R.layout.activity_welcome)
        imageView5= findViewById(R.id.imageView5)
        backgoundAll = findViewById<View>(R.id.viewBackground)

    }

    fun OpenFragment(){
        val expandableLayout = findViewById<ExpandableLayout>(R.id.expandable_layout)
        expandableLayout.toggle()

    }
}