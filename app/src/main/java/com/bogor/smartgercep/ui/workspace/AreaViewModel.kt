package com.bogor.smartgercep.ui.workspace

import android.util.Log
import androidx.lifecycle.*
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bogor.smartgercep.model.AreaModel
import com.bogor.smartgercep.network.Resource
import com.bogor.smartgercep.repository.AreaRepository
import com.bogor.smartgercep.responses.AreaResponse
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import io.realm.Realm
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AreaViewModel @Inject constructor(
    private val areaRepository: AreaRepository
) : ViewModel() {
    private var realm: Realm? = null

    var areaData : MutableList<AreaModel> = arrayListOf()
    private val _areaResponse : MutableLiveData<Resource<AreaResponse>> = MutableLiveData()
    val areaResponse: LiveData<Resource<AreaResponse>>
        get() = _areaResponse
    init {
        realm = Realm.getDefaultInstance()
        getAllArea()
    }

    fun getAllArea(){
        Log.d("asd","ini")
        val area = realm?.where(AreaModel::class.java)?.findAll()
        if (area!!.size < 1){
            getAreaFromApi()
        }
        areaData?.addAll(area)
    }
     fun saveAreaFromDb(dataArea: AreaResponse) {
        val gson = Gson()
        val jsonString = gson.toJson(dataArea)
        realm?.beginTransaction()
        realm?.delete(AreaModel::class.java)
        realm?.createAllFromJson(AreaModel::class.java,jsonString)
        realm?.commitTransaction()

    }

    override fun onCleared() {
        realm!!.close()
        super.onCleared()
    }

    private fun getAreaFromApi()=viewModelScope.launch{
        _areaResponse.value = areaRepository.getArea()
    }
}
class AreaViewModelFactory @Inject constructor(private val areaRepository: AreaRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AreaViewModel(areaRepository) as T
    }
}