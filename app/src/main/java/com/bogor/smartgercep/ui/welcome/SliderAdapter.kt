package com.bogor.smartgercep.ui.welcome

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bogor.smartgercep.R

class SliderAdapter(private val sliderItem: List<SliderItem>): RecyclerView.Adapter<SliderAdapter.SliderViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderViewHolder {
        return SliderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.slider_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SliderViewHolder, position: Int) {
        holder.bind(sliderItem[position])
    }

    override fun getItemCount(): Int {
        return sliderItem.size
    }
    inner class SliderViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val textTitle = view.findViewById<TextView>(R.id.titleSlide)
        private val textSubTitle = view.findViewById<TextView>(R.id.subTitle)
        private val imageView = view.findViewById<ImageView>(R.id.imageView)

        fun bind(sliderItem: SliderItem){
            textTitle.text = sliderItem.title
            textSubTitle.text = sliderItem.subTitile
            imageView.setImageResource(sliderItem.icon)
        }
    }



}