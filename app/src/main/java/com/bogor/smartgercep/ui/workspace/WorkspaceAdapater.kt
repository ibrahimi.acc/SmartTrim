package com.bogor.smartgercep.ui.workspace

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bogor.smartgercep.R
import com.bogor.smartgercep.model.WorkspaceModel

class WorkspaceAdapater(
    val context: Context,
    private val listener: OnItemClickListener
): RecyclerView.Adapter<WorkspaceAdapater.WorkspaceViewHolder>() {

    private val workspace: MutableList<WorkspaceModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkspaceViewHolder {
        return WorkspaceViewHolder(LayoutInflater.from(context).inflate(R.layout.item_workspace,parent,false))
    }

    override fun onBindViewHolder(holder: WorkspaceViewHolder, position: Int) {
        holder.bindModel(workspace[position])
    }
    fun getUId(position: Int): String? {
        val text = workspace[position].id
        return text
    }
    override fun getItemCount(): Int {
        return  workspace.size
    }
    fun setWorkspace(data: List<WorkspaceModel>){
        workspace.clear()
        workspace.addAll(data)
        notifyDataSetChanged()
    }
    inner class WorkspaceViewHolder(i: View): RecyclerView.ViewHolder(i), View.OnClickListener{
        val tv_workspace : TextView = i.findViewById(R.id.lv_workspacename)
        val tv_rtrw : TextView = i.findViewById(R.id.lv_rtrw)

        fun bindModel(u: WorkspaceModel){
            tv_workspace.text = u.workspaceName.toString()
            tv_rtrw.text = "${u.desaName} | RT/RW : ${u.rt.toString()}/${u.rw.toString()}"

        }
        init {
            i.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position: Int = adapterPosition
            if(position != RecyclerView.NO_POSITION){
                listener.onItemClick(position)
            }
        }

    }
    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }


}