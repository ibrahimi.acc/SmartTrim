package com.bogor.smartgercep.ui.users

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import com.bogor.smartgercep.DataStoreViewModel
import com.bogor.smartgercep.databinding.FragmentLoginBinding
import com.bogor.smartgercep.network.Resource
import com.bogor.smartgercep.ui.welcome.WelcomeActivity
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint


/*
* UsersViewModel()
* ViewModelProvider
 */
@AndroidEntryPoint
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding?  = null
    private val binding get() = _binding!!
    private val dataStoreViewModel: DataStoreViewModel by viewModels()
    private val viewModel: UsersViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        reenterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, /* forward= */ false)
    }

    override fun onDestroy() {

        (activity as WelcomeActivity?)?.OpenFragment()
        super.onDestroy()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)

        binding.forgetPassword.setOnClickListener {
            Toast.makeText(requireContext(), "Hello", Toast.LENGTH_SHORT).show()
        }
        binding.textRegister.setOnClickListener {
//            (activity as UsersActivity?)?.changetoRegister(1)
        }
        binding.containedButton.setOnClickListener {
            if(binding.outlinedTextFieldEmail.editText?.text.toString().trim() != "" &&
                binding.outlinedTextFieldEmail.error==null
                && binding.outlinedTextFieldPassword.editText?.text.toString().trim() != ""
                && binding.outlinedTextFieldPassword.error == null
            ){
                viewModel.login(
                    binding.outlinedTextFieldEmail.editText?.text.toString(),
                    binding.outlinedTextFieldPassword.editText?.text.toString()
                )
            }else{
                Toast.makeText(requireContext(), "Mohon Periksa Inputan Anda", Toast.LENGTH_SHORT).show()
            }
        }
        viewModel.emailInput?.observe(viewLifecycleOwner,{
            if (!it.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(it).matches()) {
                binding.outlinedTextFieldEmail.error = null

            } else {
                binding.outlinedTextFieldEmail.error = "Email Anda Tidak valid"

            }
        })
        binding.inputEmail.addTextChangedListener { it ->
            viewModel.setEmail(it.toString())
        }

        binding.outlinedTextFieldPassword.editText?.addTextChangedListener {
            if(it.toString().trim().isEmpty()){
                binding.outlinedTextFieldPassword.error = "Password Tidak Boleh Kosong"
            }else if(it.toString().length<6){
                binding.outlinedTextFieldPassword.error = "Password Tidak Boleh Kurang Dari 6 Karakter"
            }
            else{
                binding.outlinedTextFieldPassword.error = null
            }
        }
        viewModel.loginResponse.observe(viewLifecycleOwner,{
            when(it){
                is Resource.Success -> {
                    dataStoreViewModel.setUserData(
                        it.value?.user.role.toSet(),
                        it.value.user._id,
                        it.value.user.nik,
                        it.value.user.name,
                        it.value.token,
                        it.value.user.email
                    )
                    requireActivity().finish()
                }
                is Resource.Failure -> {
                    Toast.makeText(requireContext(),"Email atau password salah", Toast.LENGTH_LONG).show()
                }
            }
        })
        val root: View = binding.root
        return root
    }

}