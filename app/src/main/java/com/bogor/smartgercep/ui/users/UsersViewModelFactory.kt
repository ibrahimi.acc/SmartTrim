package com.bogor.smartgercep.ui.users

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bogor.smartgercep.repository.AuthRepository
import javax.inject.Inject

class UsersViewModelFactory @Inject constructor(private val authRepository: AuthRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return UsersViewModel(authRepository) as T
    }
}