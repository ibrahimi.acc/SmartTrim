package com.bogor.smartgercep.util

import android.content.Context
import android.location.Location
import androidx.core.content.ContextCompat
import com.bogor.smartgercep.R
import com.bogor.smartgercep.util.maputil.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ui.IconGenerator
import java.util.*


fun Context.defaultMarker(number: Int): MarkerOptions {
    return MarkerOptions()
        .icon(BitmapDescriptorFactory.fromBitmap(defaultIconGenerator(R.drawable.ic_marker_pin).makeIcon(number.toString())))
        .zIndex(1f)
        .title(number.toString())
}

fun Context.defaultIconGenerator(drawable: Int): IconGenerator {
    val iconGenerator = IconGenerator(this)
    iconGenerator.setTextAppearance(R.style.MarkerTitle)
    iconGenerator.setBackground(getDrawable(drawable))
    return iconGenerator
}
fun Context.generateLabelBetween(latLng0: LatLng, latLng1: LatLng): MarkerOptions {
    val distanceLabel = IconGenerator(this)
    val distance = latLng0.distanceTo(latLng1)
    val bearing = latLng0.computeBearing(latLng1)
    val center = latLng0.getNewCoordinateWith(
        bearing.toPositiveDegree(),
        (latLng0.distanceTo(latLng1) / 2).toDouble()
    )
    distanceLabel.setBackground(ContextCompat.getDrawable(this, R.drawable.transparent))
    distanceLabel.setTextAppearance(R.style.PolyLineLabelTextStyle)
    distanceLabel.setContentPadding(0, 0, 0, 20)
    return MarkerOptions()
        .icon(BitmapDescriptorFactory.fromBitmap(distanceLabel.makeIcon("%.2f m".format(Locale.ENGLISH, distance))))
        .position(center)
        .anchor(distanceLabel.anchorU, distanceLabel.anchorV)
}
fun LatLng.distanceTo(destination: LatLng): Float {
    val results = FloatArray(1)
    Location.distanceBetween(
        latitude,
        longitude,
        destination.latitude,
        destination.longitude,
        results
    )
    return results[0]
}
