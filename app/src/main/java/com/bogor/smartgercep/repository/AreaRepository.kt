package com.bogor.smartgercep.repository

import com.bogor.smartgercep.network.AreaApi
import javax.inject.Inject

class AreaRepository @Inject constructor(
    private val api: AreaApi
) : BaseRepository() {
    suspend fun getArea(
    ) = safeApiCall {
        api.getArea()
    }
}