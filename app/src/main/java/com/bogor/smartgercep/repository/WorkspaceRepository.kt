package com.bogor.smartgercep.repository

import com.bogor.smartgercep.network.WorkspaceApi
import javax.inject.Inject

class WorkspaceRepository @Inject constructor(
    private val api: WorkspaceApi,
    ): BaseRepository() {
    suspend fun insertWorkspace(
        sync: String,
        name : String,
        wilayahId : String,
        rt: String,
        rw: String
    ) = safeApiCall {
        api.insertWorkspace(sync, name, wilayahId,rt,rw)
    }
}