package com.bogor.smartgercep

import androidx.lifecycle.*
import com.bogor.smartgercep.preferences.PreferenceStorage
import com.bogor.smartgercep.preferences.UserPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DataStoreViewModel @Inject constructor(private val preferenceStorage: PreferenceStorage): ViewModel(){
    var getToken: LiveData<UserPreferences>
    init {
        getToken = preferenceStorage.getToken().asLiveData()
    }
    //saved key as liveData
    val hasTour = preferenceStorage.hasTour().asLiveData()
    fun setHasTour(key: Boolean) {
        viewModelScope.launch {
            preferenceStorage.setHasTour(key)
        }
    }

    fun setUserData(role:Set<String>,id:String,nik:String,name:String,token:String,email:String) {
        viewModelScope.launch {
            preferenceStorage.setAccount(role,id,nik,name,token,email)
        }
    }


}

class DataStoreViewModelFactory @Inject constructor(private val preferenceStorage: PreferenceStorage): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DataStoreViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DataStoreViewModel(preferenceStorage) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}