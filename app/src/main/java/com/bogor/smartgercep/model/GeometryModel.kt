package com.bogor.smartgercep.model;

import io.realm.annotations.RealmClass;

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

@RealmClass
open class GeometryModel(

) : RealmObject() {
    @PrimaryKey
    var uid: String? = null
    var workspace: WorkspaceModel? = null
    var type: String = ""
    var image: String = ""
    var hasSync: Boolean = false
    var syncId: String = ""
    var points: RealmList<PointModel>? = null
}