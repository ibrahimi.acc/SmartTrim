package com.bogor.smartgercep.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class WorkspaceModel(
) : RealmObject(){
    @PrimaryKey
    var id: String? = null

    var workspaceName: String? = null
    var rt: String? = null
    var rw: String? = null
    var syncWorkspace:String =""
    var desaName: String = ""
    var wilayahId: String? = null
    var hasSync:Boolean = false
    var createAt: Date = Date()
}