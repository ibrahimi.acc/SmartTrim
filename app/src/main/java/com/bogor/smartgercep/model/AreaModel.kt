package com.bogor.smartgercep.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class AreaModel(

) : RealmObject(){
    var _id: String? = null
    var id: String? = null
    var kode: String? = null
    var nama: String? = null
    var induk: String? = null
    var tipe: String? = null
    override fun toString(): String {
        return nama.toString()
    }
}