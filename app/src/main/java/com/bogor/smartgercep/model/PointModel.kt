package com.bogor.smartgercep.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import io.realm.annotations.Required
import java.util.*

@RealmClass
open class PointModel (
): RealmObject(){
    @PrimaryKey
    var uid: String? = null

    @Required
    var lat: Double? = 0.0

    @Required
    var lon: Double? = 0.0

    @Required
    var number: Int? = 0

    @Required
    var altitude: Double? = 0.0

    @Required
    var accuracy: Double? = 0.0

    var createAt: Date? = Date()
    var updateAt: Date? = Date()

    @Required
    var distanceToGps: Double? = 0.0

    var hasSync:Boolean= false
    var geometryModel:GeometryModel? =  null

}