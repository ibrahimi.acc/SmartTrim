package com.bogor.smartgercep.global

import android.content.Context
import androidx.datastore.preferences.core.stringPreferencesKey
import com.bogor.smartgercep.network.AreaApi
import com.bogor.smartgercep.network.AuthApi
import com.bogor.smartgercep.network.RemoteDataSource
import com.bogor.smartgercep.network.WorkspaceApi
import com.bogor.smartgercep.preferences.dataStore
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {


    @Provides
    fun provideAuthApi(@ApplicationContext context: Context, remoteDataSource: RemoteDataSource): AuthApi{
        return remoteDataSource.buildApi(AuthApi::class.java)
    }
    @Provides
    fun provideAreaApi(@ApplicationContext context: Context, remoteDataSource: RemoteDataSource): AreaApi{

        return remoteDataSource.buildApi(AreaApi::class.java, runBlocking { context.dataStore?.data?.first()?.get(stringPreferencesKey("token")) })
    }
    @Provides
    fun provideWorkspaceApi(@ApplicationContext context: Context, remoteDataSource: RemoteDataSource): WorkspaceApi{

        return remoteDataSource.buildApi(WorkspaceApi::class.java, runBlocking { context.dataStore?.data?.first()?.get(stringPreferencesKey("token")) })
    }
    @Singleton
    @Provides
    fun providePlacesClient(@ApplicationContext context: Context): PlacesClient =
        Places.createClient(context)
    @ApplicationScope
    @Provides
    @Singleton
    fun providesApplicationScope() = CoroutineScope(SupervisorJob())
}
@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationScope