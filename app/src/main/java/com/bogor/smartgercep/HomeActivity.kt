package com.bogor.smartgercep

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.bogor.smartgercep.databinding.ActivityHomeBinding
import com.bogor.smartgercep.ui.welcome.WelcomeActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


/*
* dataStoreViewModel()
* ViewModelProvider
 */
@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
//    private val dataStoreViewModel: DataStoreViewModel by viewModels()
    private val viewModel: DataStoreViewModel by viewModels()
    private fun setWindowFlag(bits: Int, on: Boolean) {
        val win = window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }
    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navView: BottomNavigationView = binding.navView
//        StatusBarCompat.setStatusBarColor(this, Color.TRANSPARENT)

        val account=navView.menu.findItem(R.id.navigation_account)
        val workspacemenu = navView.menu.findItem(R.id.navigation_workspace)
        workspacemenu.isVisible = false
        viewModel.getToken?.observe(this,{
            Log.d("asd","${it}")
            if(it?.token=="" || it?.token==null){
                Log.d("asd","${it?.token}")
                account.setTitle("Masuk")

            }else{
                account.setTitle("Akun")
                Log.d("asd","${it?.role!!.contains("admin")}")
                if(it?.role!!.contains("admin") || it?.role!!.contains("surveyor")){
                    workspacemenu.isVisible = true
                }
            }

        })
        account.setOnMenuItemClickListener {
            if(it.title=="Masuk"){
                val intent = Intent(this, WelcomeActivity::class.java)
                startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
                true
            }
            false
        }

//        navView.setOnNavigationItemSelectedListener { item ->
//            Log.d("asd","baim")
//            when(item.itemId) {
//                R.id.navigation_account -> {
//                    Log.d("asd","qkwjdbkqjwbd")
//                    true
//                }
//                else -> false
//            }
//        }
        val navController = findNavController(R.id.nav_host_fragment_activity_home)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }


}