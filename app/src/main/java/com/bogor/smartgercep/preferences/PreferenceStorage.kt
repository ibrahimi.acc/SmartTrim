package com.bogor.smartgercep.preferences

import kotlinx.coroutines.flow.Flow

interface PreferenceStorage {
    fun hasTour() : Flow<Boolean>
    suspend fun setHasTour(order: Boolean)
    suspend fun setAccount(
        role: Set<String>,
        id: String,
        nik: String,
        name: String,
        token: String,
        email:String
    )

    fun getToken(): Flow<UserPreferences>
}