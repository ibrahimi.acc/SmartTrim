package com.bogor.smartgercep.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import com.bogor.smartgercep.preferences.PreferenceImpl.PreferencesKeys.EMAIL
import com.bogor.smartgercep.preferences.PreferenceImpl.PreferencesKeys.ID
import com.bogor.smartgercep.preferences.PreferenceImpl.PreferencesKeys.NAME
import com.bogor.smartgercep.preferences.PreferenceImpl.PreferencesKeys.ROLE
import com.bogor.smartgercep.preferences.PreferenceImpl.PreferencesKeys.TOKEN
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "data_smartTrim")
data class UserPreferences @Inject constructor(
    val email: String,
    val id: String,
    val fullname: String,
    val token: String,
    val role: Set<String>?
)
@Singleton
class PreferenceImpl @Inject constructor(@ApplicationContext context: Context) : PreferenceStorage {

    private val dataStore = context.dataStore

    private object PreferencesKeys {
        val HAS_TOUR = booleanPreferencesKey("has_tour")
        val ROLE = stringSetPreferencesKey("role")
        val ID = stringPreferencesKey("id")
        val NIK = stringPreferencesKey("nik")
        val NAME = stringPreferencesKey("name")
        val EMAIL = stringPreferencesKey("email")
        val TOKEN = stringPreferencesKey("token")
    }

    //get saved key
    override fun hasTour() = dataStore.data.catch { it ->
        if (it is IOException) {
            emit(emptyPreferences())
        } else {
            throw it
        }
    }.map {
        it[PreferencesKeys.HAS_TOUR] ?: false
    }

    //set saved key
    override suspend fun setHasTour(has_tour: Boolean) {
        dataStore.edit {
            it[PreferencesKeys.HAS_TOUR] = has_tour

        }
    }

    override suspend fun setAccount(
        role: Set<String>,
        id: String,
        nik: String,
        name: String,
        token: String,
        email: String) {
        dataStore.edit {
            it[PreferencesKeys.ROLE] = role
            it[PreferencesKeys.ID] = id
            it[PreferencesKeys.NIK] = nik
            it[PreferencesKeys.NAME] = name
            it[PreferencesKeys.TOKEN] = token
            it[PreferencesKeys.EMAIL] = email

        }
    }

    override fun getToken(): Flow<UserPreferences> = dataStore.data.catch { exception ->
        if (exception is IOException) {
            emit(emptyPreferences())
        } else {
            throw exception
        }
    }.map { preference ->
        val email= preference[EMAIL] ?: ""
        val id= preference[ID] ?: ""
        val fullname= preference[NAME] ?: ""
        val token=preference[TOKEN] ?: ""
        val role= preference[ROLE] ?: null
        UserPreferences(email, id, fullname, token, role)
    }

}