package com.bogor.smartgercep.network

import com.bogor.smartgercep.responses.WorkspaceResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface WorkspaceApi  {

    @FormUrlEncoded
    @POST("workspace/insert")
    suspend fun insertWorkspace(
        @Field("sync") sync: String,
        @Field("name") name: String,
        @Field("wilayahId") wilayahId: String,
        @Field("rt") rt: String,
        @Field("rw") rw: String
    ) : WorkspaceResponse
}