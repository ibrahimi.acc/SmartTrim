package com.bogor.smartgercep.network

import com.bogor.smartgercep.responses.AreaResponse
import retrofit2.http.GET

interface AreaApi  {

    @GET("wilayah/all")
    suspend fun getArea(
    ) : AreaResponse
}