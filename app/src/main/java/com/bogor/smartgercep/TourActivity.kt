package com.bogor.smartgercep

import android.app.ActivityOptions
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.bogor.smartgercep.databinding.ActivityTourBinding
import com.bogor.smartgercep.ui.welcome.SliderAdapter
import com.bogor.smartgercep.ui.welcome.SliderItem
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator
import dagger.hilt.android.AndroidEntryPoint


/*
* DataStoreViewModel()
* */

@AndroidEntryPoint
class TourActivity : AppCompatActivity() {
    private val dataStoreViewModel: DataStoreViewModel by viewModels()
    private lateinit var binding: ActivityTourBinding

    lateinit var viewPager2: ViewPager2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTourBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        checkHasTour()
        setTheme(R.style.Theme_SmartTrim)
        supportActionBar?.hide()

        viewPager2 = findViewById(R.id.viewPageSlider)
        val textNext = findViewById<TextView>(R.id.textNextorDone)
        val tourIndicator = findViewById<WormDotsIndicator>(R.id.dots_indicatorTour)
        val sliderItem : MutableList<SliderItem> = arrayListOf()
        sliderItem.add(
            SliderItem(
                "Surveyor",
                "Jadilah Surveyor, pengambilan data geometry dengan digital",
                R.drawable.survoyor
        )
        )
        sliderItem.add(SliderItem(
            "Jasa dan Barang",
            "Dapatkan Jasa, barang baru atau bekas disekitar anda",
            R.drawable.service
        ))
        sliderItem.add(SliderItem(
            "Portal Berita",
            "Portal berita tetang bogor yang selalu terupdate",
            R.drawable.news
        ))
        viewPager2.adapter = SliderAdapter(sliderItem)
        textNext.setOnClickListener {
            if (textNext.text=="Selanjutnya"){
                viewPager2.currentItem=viewPager2.currentItem.plus(1)
            }else{
                dataStoreViewModel.setHasTour(true)

            }
        }
        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageSelected(position: Int) {
                if(position ==2){
                    textNext.text="Mulai"

                }else{
                    textNext.text="Selanjutnya"
                }
                super.onPageSelected(position)
            }
        })
        tourIndicator.setViewPager2(viewPager2)

    }
    private fun checkHasTour(){
        dataStoreViewModel.hasTour.observe(this){
            if (it == true){
                //saved go to the next activity
                    finish()
                val intent = Intent(this@TourActivity, HomeActivity::class.java)
                startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this@TourActivity).toBundle())
            }
        }
    }
}