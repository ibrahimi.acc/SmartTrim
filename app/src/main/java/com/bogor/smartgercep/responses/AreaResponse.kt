package com.bogor.smartgercep.responses

class AreaResponse : ArrayList<AreaResponseItem>()
data class AreaResponseItem(
    val _id: String,
    val id: String,
    val induk: String,
    val kode: String,
    val nama: String,
    val tipe: String
)