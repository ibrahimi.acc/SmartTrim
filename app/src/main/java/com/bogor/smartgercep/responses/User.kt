package com.bogor.smartgercep.responses

data class User(
    val _id: String,
    val active: Boolean,
    val createdAt: String,
    val email: String,
    val name: String,
    val nik: String,
    val role: List<String>,
    val updatedAt: String
)