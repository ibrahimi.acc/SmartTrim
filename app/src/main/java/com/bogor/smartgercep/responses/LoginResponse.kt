package com.bogor.smartgercep.responses

data class LoginResponse(
    val token: String,
    val user: User
)