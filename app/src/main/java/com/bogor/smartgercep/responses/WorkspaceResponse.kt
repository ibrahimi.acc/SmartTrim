package com.bogor.smartgercep.responses

data class WorkspaceResponse(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val name: String,
    val sync: String,
    val updatedAt: String,
    val user: String,
    val verif: Boolean
)